package edu.unk.fun330.controllers;

import java.awt.*;
import java.util.Random;
import java.util.Vector;

import edu.unk.fun330.*;
import edu.unk.fun330.base.*;

import static java.lang.Math.sin;

public class ChristianShipController extends ShipController{

    public ChristianShipController(Ship ship) {
        super(ship);
    }

    @Override
    public String getName() {
        return "Christian's";
    }



    private final int MAX_MOVE_COUNT = 4;
    private int fancy = 0;
    private int total = 0;
    private int accelCount = 0;
    private FlyingObject target = null;
    private FlyingObject shootTarget = null;
    boolean alternateShooting = false;
    private Universe univ;
    @Override
	/*
	 * This is the method that the engine calls to get the next move of the
	 * ship. It can call other methods and then return the move to make.
	 */
    public ControllerAction makeMove(Universe u) {
        this.univ = u;
        //System.out.println(testFirable(00, 00));

        Vector<FlyingObject> objs = u.getFlyingObjects();



        if ((this.getShip().shieldUp() && !checkForShield(u)) ||
                !this.getShip().shieldUp() && this.getShip().canShield() && checkForShield(u)){
            ToggleShield p = new ToggleShield();

            return new ToggleShield();
        }

        float futureX = this.getShip().getNextX(10);
        float futureY = this.getShip().getNextY(10);

        final float accel = 10f;
        if (this.ship.getBulletsAmmount() > 0 && ((futureY > Universe.getHeight() || futureX > Universe.getWidth() || futureX < 0 || futureY < 0)
                || (this.getShip().getSpeed() == 0 && this.getShip().getFuelAmmount() > 0))){
            FlightAdjustment fa = new FlightAdjustment();

            target = findTarget(u);
            //double distance = Math.sqrt(Math.pow(this.getShip().getX() - target.getX(), 2) + Math.pow(this.getShip().getY() - target.getY(), 2));

            fa.setAcceleration(accel); //(float)magnitude);

            double newFacing = (float) Math.atan2((target.getY() - this.getShip().getY()), (target.getX() - this.getShip().getX()));

            fa.setFacing((float)newFacing);
            accelCount = MAX_MOVE_COUNT;
            if (this.getShip().getSpeed() != 0) {
                //fa.setStop(true);
            }
            fa = addPolarVectors(ship.getHeading(), -ship.getAcceleration()*2, fa.getFacing(), fa.getAcceleration());
            fa.setAcceleration(accel);
            //System.out.println(change.getFacing() / Math.PI * 180);
            //System.out.println(addPolarVectorsB(ship.getHeading(), ship.getAcceleration(), fa.getFacing(), fa.getAcceleration()).getFacing() / Math.PI * 180);
            return fa;
        }

        if (accelCount > 0){

            accelCount--;
            //change.setStop(false);
            float newFacing = (float) Math.atan2((target.getY() - this.getShip().getY()), (target.getX() - this.getShip().getX()));
            FlightAdjustment fa =addPolarVectors(ship.getHeading(), -ship.getSpeed(), newFacing, 10f);
            if (Math.abs(fa.getFacing() - ship.getHeading()) > .30){
                accelCount++;
            }
            return fa;
        }

        alternateShooting = !alternateShooting;
        if (ship.canFire() && (shootTarget != null && shootTarget == scanForFirable(u))){
            return new FireBullet();
        } else {


            shootTarget = scanForFirable(u);

            if (shootTarget == null){
                return null;
            }
            float newFacing = checkCanFire((Ship)shootTarget);
            if (newFacing == Float.MAX_VALUE){
                newFacing = (float) Math.atan2((shootTarget.getY() - this.getShip().getY()), (shootTarget.getX() - this.getShip().getX()));
            } else {
                fancy++;
            }
            total++;
            FlightAdjustment fa = new FlightAdjustment();
            fa.setFacing(newFacing);
            fa.setAcceleration(0);
            return fa;
        }

        /*
        if (ship.canFire()){
            if (shootTarget == null || !testFirable((Ship)shootTarget)) {
                shootTarget = scanForFirable(u);
                return null;
            } else {
                if (testFirable((Ship)shootTarget)) {
                    return new FireBullet();
                }
                return null;
            }
        } else {

        }*/
        // return null;
    }
    private float findBulletSpeed(Ship ship, float facing){
        float pi = (float)Math.PI;

        float heading =(float)( ship.getHeading() % (2*Math.PI) );

        if (facing < 0)
            facing = 2*pi + facing;
        if (heading < 0)
            heading = 2*pi + heading;

        float hMf = heading - facing,
                fMh = facing - heading;

        if(heading > facing){
            if (hMf < pi/4){//add speed
                return 8f + ship.getSpeed();
            }
            else if (hMf > 3*pi/4 && hMf < 5*pi/4){//minus speed
                return 8f - ship.getSpeed();
            }
            else return 8f;
        }//end if
        else{
            if (fMh > 3*pi/4 && fMh < 5*pi/4){//minus speed
                return 8f - ship.getSpeed();
            }
            else if (fMh < pi/4){//add speed
                return 8f + ship.getSpeed();
            }
            else return 8f;
        }//end else
    }//end findSpeed

    private boolean testFirable(float destX, float destY){
        // bullet speed: 8f + ship.Speed
        final int BULLET_LIFE = 20;
        float heading = ship.getHeading();
        float speed = findBulletSpeed(this.getShip(), heading);
        float x= ship.getX();
        float y= ship.getY();

        float destdist = (float)Math.sqrt(
                Math.pow(destY - y, 2) +
                        Math.pow(destX - x, 2)
        );

        float bulletDist =  (float)Math.sqrt(
                Math.pow(speed * BULLET_LIFE * (float) Math.cos(heading), 2) +
                        Math.pow(speed * BULLET_LIFE * (float) sin(heading), 2)
        );

        return (destdist < bulletDist);
    }

    private boolean testFirable(Ship ship){
        return (checkCanFire(ship) != Float.MAX_VALUE);
    }

    /**
     * determine if this ship can hit another ship
     * @param goal the target to hit
     * @return whether it can be hit
     * @see <a href='http://www.wolframalpha.com/input/?i=solve+s1+%2B+x1+%2B+T1+cos(h1)+%3D+x2+%2B+s2+T2+cos(h2);+s1+%2B+y1+%2B+T1+sin(h1)+%3D+y2+%2B+s2+T2+sin(h2)+for+T1+and+T2'>wolfram alpha</a>
     */
    private float checkCanFire(Ship goal){
        final float MAX_DIST = goal.getRadius() + 6;

        float bestDist = Float.MAX_VALUE;
        float bestAngle = 0;
        for(int frame = 1; frame <= 20; frame++){
            float targetx = goal.getNextX(frame+1);
            float targety = goal.getNextY(frame+1);

            float angle = Util.findAngle(ship.getX(), ship.getY(), targetx, targety);
            float speed = findBulletSpeed(this.ship, angle);

            float endX = this.ship.getX() + speed * frame * (float) Math.cos(angle);
            float endY = this.ship.getY() + speed * frame * (float) Math.sin(angle);

            float dist = Util.distance(targetx, targety, endX, endY);
            if (dist < MAX_DIST && dist < bestDist){
                bestDist = dist;
                bestAngle = angle;
            }
        }
        if (bestDist != 0 && bestDist < MAX_DIST){
            return bestAngle;
        } else {
            return Float.MAX_VALUE;
        }
    }

    private FlyingObject scanForFirable(Universe u){
        Vector<FlyingObject> objs = u.getFlyingObjects();

        final int DISTANCE_LIMIT = 300;

        Ship bestTarget = null;
        for(FlyingObject x : objs){
            //if (x.)
            if (x instanceof Ship && x != ship //&& testFirable((Ship)x) //x.getNextX(20), x.getNextY(20))
                    && (bestTarget == null || ((Ship)x).getShieldHealth() == 0 ||
                    Util.distance(this.ship.getX(), this.ship.getY(), x.getX(), x.getY()) < Util.distance(this.ship.getX(), this.ship.getY(), bestTarget.getX(), bestTarget.getY()))){
                bestTarget = (Ship)x;
            }
        }

        if (bestTarget == null){
            return null;
        }

        // to get better, remove testFirable, add prediction for it

        return (Util.distance(this.ship.getX(), this.ship.getY(), bestTarget.getX(), bestTarget.getY()) < DISTANCE_LIMIT) ? bestTarget : null;
    }

    public boolean checkForShield(Universe u){
        final int depth = 6;
        Vector<FlyingObject> objs = u.getFlyingObjects();

        for(FlyingObject x : objs){
            //if (x.)
            if ((x instanceof Ship || x instanceof Bullet) && x != this.getShip()){
                for(int i = 0; i < depth; i++) {
                    if (intersectsIn(x, i)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     *
     * @see <a href='https://math.stackexchange.com/questions/1365622/adding-two-polar-vectors'>stackexchange math</a>
     * @return Resultant
     */
    private FlightAdjustment addPolarVectors(float angleA, float magA, float angleB, float magB){
        FlightAdjustment fa  = new FlightAdjustment();
        fa.setAcceleration((float)Math.sqrt(magA * magA + magB * magB + 2 * magA*magB*Math.cos(angleB-angleA)));
        fa.setFacing(angleA + (float)Math.atan2(magB * sin(angleB - angleA), magA + magB*Math.cos(angleB-angleA)));
        return fa;
    }



    private float distanceIn (FlyingObject fo, int dist) {
        return Util.distance(this.getShip().getNextX(dist), this.getShip().getNextY(dist), fo.getNextX(dist), fo.getNextY(dist) );
    }

    public boolean intersectsIn (FlyingObject fo, int count) {
        float distance = this.distanceIn(fo, count);
        //If there is a collision
        return (distance < this.getShip().getRadius() + fo.getRadius());
    }

    public FlyingObject findTarget(Universe u){
        FlyingObject fo = null;
        for (FlyingObject ob : u.getFlyingObjects()) {
            if (ob == this.getShip()){ continue; }
            if (ob instanceof Bonus) {
                fo = ob;
                break;
            } else if (ob instanceof Ship) {
                if (fo instanceof Ship){
                    if (((Ship)fo).getScore() > ((Ship)ob).getScore()){
                        fo = ob;
                    }
                } else {
                    fo = ob;
                }
            } else if (ob instanceof Bullet) {

            } else {
                //System.out.println(ob.getClass().getName());
            }
        }
        return fo;
    }

	/*
	@Override
	public Bullet fireBullet(Universe gd) {
		Bullet bullet = new Bullet(ship.getX()+5,ship.getY()+5);

		Random rnd = new Random();

		bullet.setFacing(6.28318531f * rnd.nextFloat());
		bullet.setHeading(6.28318531f * rnd.nextFloat());
		bullet.setSpeed(1.0f);

		return bullet;
	}
	*/



}
